@extends('layouts.app')
@section('content')

    <h1>Editar datos de Estudio</h1>
    <div class="form">
    <form  action="/study/{{ $study->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Familia: </label>
        <select type="select" name="id_study_family" value="{{ old('id_study_family') }}">
            @foreach ($studiesFamily as $family)
            <option value="{{ $family->id }}" {{ $study->family->id == $family->id ?: old('family') == $family->id ? 'selected' : '' }}>{{ $family->name }} >{{ $family->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_study_family') }}
    </div>
    <div class="form-group">
        <label>Tipo de Estudio: </label>
        <select type="select" name="id_study_type" value="{{ old('id_study_type') }}">
            @foreach ($studiesType as $type)
            <option value="{{ $type->id }}" {{ $study->type->id == $type->id ?: old('type') == $type->id ? 'selected' : '' }}type>{{ $type->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_study_family') }}
    </div>
    <div class="form-group">
        <label>Codigo: </label>
        <input type="text" name="code" value="{{ old('code',  $study->code) }}">
        {{ $errors->first('code') }}
    </div>
    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name',  $study->name) }}">
        {{ $errors->first('name') }}
    </div>
    <div class="form-group">
        <label>Nombre corto: </label>
        <input type="text" name="short_name" value="{{ old('short_name',  $study->short_name) }}">
        {{ $errors->first('short_name') }}
    </div>
    <div class="form-group">
        <label>Abreviacion: </label>
        <input type="text" name="abbreviation" value="{{ old('abbreviation',  $study->abbreviation) }}">
        {{ $errors->first('abbreviation') }}
    </div>
    <input type="submit" value="Guardar" class="btn btn-primary">
    </form>
    </div>

@endsection('content')