@extends('layouts.app')
@section('content')

        <h1>Detalle de Estudio</h1>

        <p><b>Familia:</b> {{ $study->family->name }} </p>
        <p><b>Tipo de estudio:</b> {{ $study->type->name }}</p>
        <p><b>Codigo:</b> {{ $study->code }}</p>
        <p><b>Nombre:</b> {{ $study->name }}</p>
        <p><b>Nombre corto:</b> {{ $study->short_name }}</p>
        <p><b>Abreviacion:</b> {{ $study->abbreviation }}</p>

@endsection('content')
