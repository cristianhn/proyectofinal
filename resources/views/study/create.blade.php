@extends('layouts.app')

@section('content')

<h1>Detalle de Estudio</h1>
<div class="form">
<form action="/studies" method="post">
    {{ csrf_field() }}

    <div class="form-group">
        <label>Familia: </label>
        <select type="select" name="id_study_family" value="{{ old('id_study_family') }}">
            @foreach ($studiesFamily as $studyFamily)
            <option value="{{ $studyFamily->id }}">{{ $studyFamily->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_study_family') }}
    </div>
    <div class="form-group">
        <label>Tipo de Estudio: </label>
        <select type="select" name="id_study_type" value="{{ old('id_study_type') }}">
            @foreach ($studiesType as $studyType)
            <option value="{{ $studyType->id }}">{{ $studyType->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_study_family') }}
    </div>

    <div class="form-group">
        <label>Codigo: </label>
        <input type="text" name="code" value="{{ old('code') }}">
        {{ $errors->first('code') }}
    </div>

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name') }}">
        {{ $errors->first('name') }}
    </div>

    <div class="form-group">
        <label>Nombre corto: </label>
        <input type="text" name="short_name" value="{{ old('short_name') }}">
        {{ $errors->first('short_name') }}
    </div>

    <div class="form-group">
        <label>Abreviacion: </label>
        <input type="text" name="abbreviation" value="{{ old('abbreviation') }}">
        {{ $errors->first('abbreviation') }}
    </div>


    

    <div class="form-group">
        <input type="submit" value="Guardar" class="btn btn-primary">
    </div>
</form>
</div>

@endsection('content')