@extends('layouts.app')

@section('content')

    <h1>Listado de Estudios</h1>

    @can('create', 'App\Study')
        <a href="/studies/create"><button type="button" class="btn btn-success">Nuevo</button></a>
    @else
        No puedes dar altas!!
    @endcan
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Familia</th>
                <th>Tipo de estudio</th>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Nombre corto</th>
                <th>Abreviacion</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($studies as $study)
                
                <tr>
                    <td>  {{ $study->id }} </td>
                    <td>  {{ $study->family->name }} </td>
                    <td>  {{ $study->type->name }} </td>
                    <td>  {{ $study->code }} </td>
                    <td>  {{ $study->name }} </td>
                    <td>  {{ $study->short_name }} </td>
                    <td>  {{ $study->abbreviation }} </td>
                    <td>  
                        <form method="post" action="/study/{{ $study->id }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}

                            <a href="/studyPlan/{{$study->id}}" class="btn btn-info">Ver Plan de Estudio</a>

                            @can('delete', $study)
                                <input type="submit" value="Borrar" class="btn btn-danger">
                                    
                                
                            @endcan
                            

                            @can('update', $study)
                                <a href="/study/{{ $study->id }}/edit"><button type="button" class="btn btn-warning">Editar</button></a>
                            @endcan

                            @can('view', $study)
                                <a href="/study/{{ $study->id }}"> <button type="button" class="btn btn-info">Ver</button></a>
                            @endcan
                        </form>


                    </td>
                </tr>
                
            @endforeach

        </tbody>
    </table>

    {{ $studies->render() }}

@endsection('content')