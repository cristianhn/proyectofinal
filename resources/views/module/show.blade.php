@extends('layouts.app')
@section('content')

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
	        <h1>Detalle del m&oacute;dulo</h1>
	        <p>Id: {{ $module->id }}</p>
	        <p>C&oacute;digo: {{ $module->code }}</p>
	        <p>Nombre: {{ $module->name }}</p>
	    </div>
	</div>

@endsection('content')