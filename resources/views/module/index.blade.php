@extends('layouts.app')
@section('content')
	<h1>Listado de m&oacute;dulos</h1>
	<a href="/module/create" class="btn btn-success">Nuevo m&oacute;dulo</a>
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>C&oacute;digo</th>
				<th>Nombre</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($modules as $module)
				<tr>
					<td>{{ $module->id }}</td>
					<td>{{ $module->code }}</td>
					<td>{{ $module->name }}</td>
					<td>
						<form class="form-horizontal" action="/module/{{$module->id}}" method="POST">
							{{csrf_field()}}
							<input type="hidden" name="_method" value="DELETE"/>
							<a href="/module/{{$module->id}}" class="btn btn-info">Ver</a>

							<a href="/module/{{$module->id}}/edit" class="btn btn-warning">Editar</a>
							<input type="submit" class="btn btn-danger" value="Borrar" />
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<a href="/module/create" class="btn btn-primary">Nuevo m&oacute;dulo</a>
	<div class="col-xs-12 text-center">
		{{ $modules->links() }}
	</div>
@endsection

@section('scripts')
@stop