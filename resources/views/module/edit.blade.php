@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<form action="/module/{{ $module->id }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT"/>
				<div class="form-group">
					<label for="code">C&oacute;digo: </label>
					<input type="text" class="form-control" name="code" id="code" value="{{ old('code', $module->code) }}"/>
					@if ($errors->first('code'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('code') }}
					</div>
					@endif
				</div>
				<div class="form-group">
					<label for="name">Nombre: </label>
					<input type="text" class="form-control" name="name" id="name" value="{{ old('name', $module->name) }}"/>
					@if ($errors->first('name'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('name') }}
					</div>
					@endif
				</div>
				<input type="submit" class="btn btn-primary" value="Guardar cambios"/>
			</form>
		</div>
	</div>
@endsection