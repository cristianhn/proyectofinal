@extends('layouts.app')
@section('content')
  <!-- Modal -->
  <div id="userModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Editar Usuario</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="userName">Nombre: </label>
            <input type="text" class="form-control" id="userName"/>
          </div>
          <div class="form-group">
            <label for="userEmail">Email: </label>
            <input type="email" class="form-control" id="userEmail"/>
          </div>
          <div id="newPasswordDiv" class="form-group">
            <label for="newPassword">Nueva Contraseña: </label>
            <input type="password" id="newPassword" class="form-control" placeholder="Nueva contraseña"/>
          </div>
          <div id="chkStudentDiv" class="checkbox">
            <label><input type="checkbox" id="chkStudent" /> Estudiante</label>
          </div>
          <!-- DATOS DEL ESTUDIANTE -->
      <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Datos Personales</a>
            </h4>
          </div>
          <div id="collapse1" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="form-group">
                    <label for="studentName">Nombre: </label>
                    <input type="text" class="form-control" id="studentName">
                  </div>
                  <div class="form-group">
                    <label for="studentSurname">Apellido: </label>
                    <input type="text" class="form-control" id="studentSurname">
                  </div>
                  <div class="form-group">
                    <label for="studentSurname2">Apellido 2: </label>
                    <input type="text" class="form-control" id="studentSurname2">
                  </div>
                  <div class="form-group">
                    <label for="studentPhone">Teléfono: </label>
                    <input type="text" class="form-control" id="studentPhone">
                  </div>
                  <div class="form-group">
                    <label for="studentPhone2">Teléfono 2: </label>
                    <input type="text" class="form-control" id="studentPhone2">
                  </div>
                  <div class="form-group">
                    <label>Sexo: </label>
                    <div class="radio">
                      @foreach ($genders as $i => $gender)
                      <label>
                        <input 
                          type="radio" 
                          name="studentGender" 
                          value="{{ $gender->id }}"
                          {{ $i == 1 ? 'checked="true"' : '' }}
                        /> 
                        {{ $gender->description }}
                      </label>
                      @endforeach
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="studentDocumentType">Tipo de Documento: </label>
                    <select id="studentDocumentType" class="form-control">
                      @foreach ($documentTypes as $documentType)
                        <option value="{{ $documentType->id }}">{{ $documentType->description }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="studentDocument">Documento de Identidad: </label>
                    <input type="text" class="form-control" id="studentDocument">
                  </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Datos del Centro</a>
            </h4>
          </div>
          <div id="collapse2" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                      <img id="studentImg" class="img-responsive"/>
                    </div>
                  </div>
                  <h5>Nueva Foto: </h5>
                  <div class="row">
                    <div class="col-xs-8">
                      <input type="file" class="form-control-file" id="studentPhoto"/>
                    </div>
                    <div class="col-xs-4">
                      <button id="btnSaveStudentPhoto" class="btn btn-primary">Guardar Foto</button>
                    </div>
                  </div>
              <div class="form-group">
                    <label for="studentCode">Código: </label>
                    <input type="text" class="form-control" id="studentCode" disabled="true" />
                  </div>
                  <div class="form-group">
                    <label for="studentLargeFamily">Familia Numerosa: </label>
                    <select id="studentLargeFamily" class="form-control">
                      @foreach ($largeFamilyTypes as $largeFamilyType)
                        <option value="{{ $largeFamilyType->id }}">{{ ucfirst($largeFamilyType->name) }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="studentLargeFamilyDocument">Documento de Familia Numerosa: </label>
                    <input type="text" class="form-control" id="studentLargeFamilyDocument"/>
                  </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Otros Datos</a>
            </h4>
          </div>
          <div id="collapse3" class="panel-collapse collapse">
            <div class="panel-body">
              <div class="form-group">
                    <label for="studentBirthDate">Fecha de Nacimiento: </label>
                    <input type="date" class="form-control" id="studentBirthDate">
                  </div>
              <div class="form-group">
                    <label for="studentBirthCountry">Pais de Nacimiento: </label>
                    <select id="studentBirthCountry" class="form-control">
                      @foreach ($countries as $country)
                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="studentBirthProvince">Provincia de Nacimiento: </label>
                    <input type="text" class="form-control" id="studentBirthProvince">
                  </div>
                  <div class="form-group">
                    <label for="studentBirthTown">Lugar de Nacimiento: </label>
                    <input type="text" class="form-control" id="studentBirthTown">
                  </div>
            </div>
          </div>
        </div>
      </div>
      <!-- FIN DE DATOS DEL ESTUDIANTE -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveUser">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End of Modal -->

  <div id="alertContainer"></div>
  @if ($user->isAdmin())
  <button class="btnNewUser btn btn-success">Nuevo</button>
  @endif
  <table class="table">
    <thead>
      <tr>
        <th>Id</th>
        <th>Tipo</th>
        <th>Nombre</th>
        <th>Email</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody id="userContainer">

    </tbody>
  </table>
@endsection
@section('scripts')
  @parent
  <script type="text/javascript">
    var token = '{{ csrf_token() }}';
    var api_token = '{{ $user->api_token }}';
  </script>
  <script type="text/javascript" src="/js/user/FileUploader.js"></script>
  <script type="text/javascript" src="/js/user/UserDB.js"></script>
  <script type="text/javascript" src="/js/user/script.js"></script>
@endsection