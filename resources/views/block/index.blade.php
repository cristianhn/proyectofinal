@extends('layouts.app')
@section('content')
	<h1>Listado de bloques</h1>
	<a href="/block/create" class="btn btn-success">Nuevo bloque</a>
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Id Itinerario</th>
				<th>Nombre</th>
				<th>MIN</th>
				<th>MAX</th>				
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($blocks as $block)
				<tr>
					<td>{{ $block->id }}</td>
					<td>{{ $block->id_itinerary }}</td>
					<td>{{ $block->name }}</td>
					<td>{{ $block->min }}</td>
					<td>{{ $block->max }}</td>					
					<td>
						<form class="form-horizontal" action="/block/{{$block->id}}" method="POST">
							{{csrf_field()}}
							<input type="hidden" name="_method" value="DELETE"/>
							<a href="/block/{{$block->id}}/edit" class="btn btn-warning">Editar</a>
							<input type="submit" class="btn btn-danger" value="Borrar" />
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="col-xs-12 text-center">
		{{ $blocks->links() }}
	</div>
@endsection