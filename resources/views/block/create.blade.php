@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<form action="/block" method="POST">
				{{ csrf_field() }}
				<h3>Nuevo Bloque</h3>
				<div class="form-group">
					<label for="itinerary">Itinerario: </label>
					<select id="itinerary" name="id_itinerary" class="form-control">
						@foreach ($itineraries as $itinerary)
							<option value="{{$itinerary->id}}" 
								{{ old('study') == $itinerary->id ? 'selected' : '' }}>
								{{$itinerary->name}}
							</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="name">Nombre: </label>
					<input type="text" class="form-control" name="name" id="name" value="{{ old('name')}}"/>
					@if ($errors->first('name'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('name') }}
					</div>
					@endif
				</div>
				<div class="form-group">
					<label for="min">Min: </label>
					<input type="text" class="form-control" name="min" id="min" value="{{ old('min')}}"/>
					@if ($errors->first('min'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('min') }}
					</div>
					@endif
				</div>
				<div class="form-group">
					<label for="max">Max </label>
					<input type="text" class="form-control" name="max" id="max" value="{{ old('max')}}"/>
					@if ($errors->first('max'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('max') }}
					</div>
					@endif
				</div>
				
				<input type="submit" class="btn btn-primary" value="Crear Bloque"/>
			</form>
		</div>
	</div>
@endsection