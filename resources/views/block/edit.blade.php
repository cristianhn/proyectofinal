@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<form action="/block/{{ $block->id }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT"/>
				<div class="form-group">
					<label for="id_itinerary">Id itinerario </label>
					<input type="text" class="form-control" name="id_itinerary" id="id_itinerary" value="{{ old('id_itinerary', $block->id_itinerary) }}"/>
					@if ($errors->first('id_itinerary'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('id_itinerary') }}
					</div>
					@endif
				</div>
				<div class="form-group">
					<label for="name">Nombre: </label>
					<input type="text" class="form-control" name="name" id="name" value="{{ old('name', $block->name) }}"/>
					@if ($errors->first('name'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('name') }}
					</div>
					@endif
				</div>
				<div class="form-group">
					<label for="min">Min: </label>
					<input type="text" class="form-control" name="min" id="min" value="{{ old('min', $block->min) }}"/>
					@if ($errors->first('min'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('min') }}
					</div>
					@endif
				</div>
				<div class="form-group">
					<label for="max">Max: </label>
					<input type="text" class="form-control" name="max" id="max" value="{{ old('max', $block->max) }}"/>
					@if ($errors->first('max'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('max') }}
					</div>
					@endif
				</div>
				
				<input type="submit" class="btn btn-primary" value="Guardar cambios"/>
			</form>
		</div>
	</div>
@endsection