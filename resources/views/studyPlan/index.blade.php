@extends('layouts.app')
@section('content')
    <h1>Listado de planes de estudios</h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="50%">Estudio</th>
                <th width="50%"></th>
            </tr>
        </thead>
        <tbody id="tbodyMain">
        </tbody>
    </table>
    <div class="col-xs-12 text-center">
        {{ $studies->links() }}
    </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="/js/studyPlan/studyPlan-ajax.js"> 
  </script>
@stop