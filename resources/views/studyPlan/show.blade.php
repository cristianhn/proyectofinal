@extends('layouts.app')
@section('content')
	<h1>Plan de estudios del estudio {{ $studyPlans[0]->study->name }}</h1>
	<table class="table" data-study="{{ $studyPlans[0]->study->id }}">
		<thead>
			<tr>
				<th>Id del m&oacute;dulo</th>
				<th>M&oacute;dulo</th>
				<th>Curso</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="tbodyMod">
		</tbody>
	</table>
	<div id="boton"></div>
@endsection
@section('scripts')
  <script type="text/javascript" src="/js/studyPlan/studyPlan-ajax.js"> 
  </script>
@stop