@extends('layouts.app')

@section('content')

    @can('create', 'App\Tuition')
        <a href="/student/create"><button type="button" class="btn btn-success">Nuevo</button></a>
    @else
        No puedes dar altas!!
    @endcan

    <table class="table" >
        <thead>
            <tr>
                <th>Id</th>
                <th>Usuario</th>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Primer Apell.</th>
                <!--<th>Segundo Apell.</th>-->
                <th>Telefono1</th>
                <!--<th>Telefono2</th>-->
                <th>Genero</th>
                <!--
                <th>TipoDocumento</th>
                -->
                <th>Documento</th>
                <!--
                <th>Fecha nac</th>
                <th>Lugar nac</th>
                <th>Provincia</th>
                <th>Ciudad nac</th>
                <th>Id Familia nu</th>
                <th>Doc Familia nu</th>
                -->
            </tr>
        </thead>
        <tbody>
            @foreach ($students as $student)
                
                <tr>
                    <td>  {{ $student->id }} </td>
                    <td>  {{ $student->id_user }} </td>
                    <td>  {{ $student->code }} </td>
                    <td>  {{ $student->name }} </td>
                    <td>  {{ $student->surname1 }} </td>
                    <!--<td>  {{ $student->surname2 }} </td>-->
                    <td>  {{ $student->phone1 }} </td>
                    <!--
                    <td>  {{ $student->phone2 }} </td>
                    -->
                    <td>    @if ($student->gender === 1)
                                Mujer 
                            @else
                                Hombre
                            @endif
                    </td>
                    <!--
                    <td>  {{ $student->document_type }} </td>
                    -->
                    <td>  {{ $student->document }} </td>
                    <!--
                    <td>  {{ $student->birth_date }} </td>
                    <td>  {{ $student->country->name }} </td>
                    <td>  {{ $student->province }} </td>
                    <td>  {{ $student->birth_town }} </td>
                    <td>  {{ $student->large_family_id }} </td>
                    <td>  {{ $student->large_family_doc }} </td>
                    -->
                    <td>  
                        <form method="post" action="/student/{{ $student->id }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}

                            @can('delete', $student)
                                <input type="submit" value="Borrar" class="btn btn-danger">
                            @endcan
                            

                            @can('update', $student)
                                <a href="/student/{{ $student->id }}/edit"><button type="button" class="btn btn-warning">Editar</button></a>
                            @endcan

                            @can('view', $student)
                                <a href="/student/{{ $student->id }}"> <button type="button" class="btn btn-info">Ver</button></a>
                            @endcan
                        </form>


                    </td>
                </tr>
                
            @endforeach

        </tbody>
    </table>

    {{ $students->render() }}

@endsection('content')