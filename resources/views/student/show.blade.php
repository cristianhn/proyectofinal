@extends('layouts.app')
@section('content')

        <h1>Detalle de Estudiante</h1>

        <p>Id: {{ $student->id }} </p>

        @foreach ($users as $user)
                @if($user->id == $student->id_user)
                        <p>Usuario: {{ $user->name }}</p>
                @endif
        @endforeach

        <p>Código: {{ $student->code }}</p>
        <p>Nombre: {{ $student->name }}</p>
        <p>Primer Apellido: {{ $student->surname1 }}</p>
        <p>Segundo Apellido: {{ $student->surname2 }}</p>
        <p>Teléfono: {{ $student->phone1 }}</p>
        <p>Otro Teléfono: {{ $student->phone2 }}</p>

        @foreach ($genders as $gender)
                @if($gender->id == $student->gender)
                        <p>Género: {{ $gender->description }}</p>
                @endif
        @endforeach

        <p>Fecha de Nacimiento: {{ $student->birth_day }}</p>
        <p>Ciudad de Nacimiento: {{ $student->birth_town }}</p>
        <p>Provincia: {{ $student->province }}</p>

        @foreach ($countries as $country)
                @if($country->id == $student->birth_country_id)
                        <p>País de Nacimiento: {{ $country->name }}</p>
                @endif
        @endforeach


@endsection('content')