@extends('layouts.app')

@section('content')

<div class="form" style="margin-left: 40%;">
<h1>Detalle de estudiante</h1>
<form action="/students" method="post">
    {{ csrf_field() }}

   <div class="form-group">
        <label>Usuario: </label>
        <select type="select" name="id_user" value="{{ old('id_user') }}">
            @foreach ($users as $user)
            <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_user') }}
    </div>

    <div class="form-group">
        <label>Codigo: </label>
        <input type="text" name="code" value="{{ old('code') }}">
        {{ $errors->first('code') }}
    </div>

    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name') }}">
        {{ $errors->first('name') }}
    </div>

    <div class="form-group">
        <label>Primer Apellido: </label>
        <input type="text" name="surname1" value="{{ old('surname1') }}">
        {{ $errors->first('surname1') }}
    </div>

    <div class="form-group">
        <label>Segundo Apellido: </label>
        <input type="text" name="surname2" value="{{ old('surname2') }}">
        {{ $errors->first('surname2') }}
    </div>

    <div class="form-group">
        <label>Teléfono: </label>
        <input type="text" name="phone1" value="{{ old('phone1') }}">
        {{ $errors->first('phone1') }}
    </div>

    <div class="form-group">
        <label>Segundo Teléfono (opcional): </label>
        <input type="text" name="phone2" value="{{ old('phone2') }}">
        {{ $errors->first('phone2') }}
    </div>
    <!--
    <div class="form-group">
        <label>Genero: </label>
        <input type="text" name="gender" value="{{ old('gender') }}">
        {{ $errors->first('gender') }}
        1-Mujer 2-Hombre
    </div>
    -->
    <div class="form-group">
        <label>Género: </label>
        <select type="select" name="gender" value="{{ old('gender') }}">
            @foreach ($genders as $gender)
            <option value="{{ $gender->id }}">{{ $gender->description }}</option>
            @endforeach
        </select>
        {{ $errors->first('gender') }}
    </div>
    <div class="form-group">
        <label>Tipo de Documento: </label>
        <select type="select" name="document_type" value="{{ old('document_type') }}">
            @foreach ($documents as $document)
            <option value="{{ $document->id }}">{{ $document->description }}</option>
            @endforeach
        </select>
        {{ $errors->first('document_type') }}
    </div>
    <!--
    <div class="form-group">
        <label>Tipo de documento: </label>
        <input type="text" name="document_type" value="{{ old('document_type') }}">
        {{ $errors->first('document_type') }}
    </div>
    -->
    <div class="form-group">
        <label>Nº Documento: </label>
        <input type="text" name="document" value="{{ old('document') }}">
        {{ $errors->first('document') }}
    </div>

    <div class="form-group">
        <label>Fecha de nacimiento: </label>
        <input type="text" name="birth_date" value="{{ old('birth_date') }}">
        {{ $errors->first('birth_date') }}
    </div>

    <div class="form-group">
        <label>Lugar de nacimiento: </label>
        <input type="text" name="birth_town" value="{{ old('birth_town') }}">
        {{ $errors->first('birth_town') }}
    </div>

    <div class="form-group">
        <label>Provincia: </label>
        <input type="text" name="province" value="{{ old('province') }}">
        {{ $errors->first('province') }}
    </div>
    <!--
    <div class="form-group">
        <label>Ciudad de nacimiento: </label>
        <input type="text" name="birth_town" value="{{ old('birth_town') }}">
        {{ $errors->first('birth_town') }}
    </div>
    -->
    <div class="form-group">
        <label>País de Nacimiento: </label>
        <select type="select" name="birth_country_id" value="{{ old('birth_country_id') }}">
            @foreach ($countries as $country)
            <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('birth_country_id') }}
    </div>
    <div class="form-group">
        <label>Tipo de Familia: </label>
        <select type="select" name="large_family_id" value="{{ old('large_family_id') }}">
            @foreach ($families as $family)
            <option value="{{ $family->id }}">{{ $family->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('large_family_id') }}
    </div>
    <!--
    <div class="form-group">
        <label>Id familia numerosa: </label>
        <input type="text" name="large_family_id" value="{{ old('large_family_id') }}">
        {{ $errors->first('large_family_id') }}
    </div>
    
    <div class="form-group">
        <label>Documento familia numerosa: </label>
        <input type="text" name="large_family_doc" value="{{ old('large_family_doc') }}">
        {{ $errors->first('large_family_doc') }}
    </div>
    -->

    

    <div class="form-group">
        <input type="submit" value="Guardar" class="btn btn-primary">
    </div>
</form>
</div>

@endsection('content')