@extends('layouts.app')
@section('content')

    <h1>Editar datos de estudiante</h1>
    <div class="form">
    <form  action="/student/{{ $student->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $student->id }}" readonly="readonly">
    </div>
    <!--
    <div class="form-group">
        <label>User: </label>
        <input type="text" name="id_user" value="{{ old('id_user',  $student->id_user) }}">
        {{ $errors->first('id_user') }}
    </div>
    -->
    <input type="hidden" name="id_user" value="{{$student->id_user}}">
    <div class="form-group">
        <label>Código: </label>
        <input type="text" name="code" value="{{ old('code',  $student->code) }}">
        {{ $errors->first('code') }}
    </div>
    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" value="{{ old('name',  $student->name) }}">
        {{ $errors->first('name') }}
    </div>

    <div class="form-group">
        <label>Primer Apellido: </label>
        <input type="text" name="surname1" value="{{ old('surname1',  $student->surname1) }}">
        {{ $errors->first('surname1') }}
    </div>
    <div class="form-group">
        <label>Segundo Apellido: </label>
        <input type="text" name="surname2" value="{{ old('surname2',  $student->surname2) }}">
        {{ $errors->first('surname2') }}
    </div>

    <div class="form-group">
        <label>Teléfono: </label>
        <input type="text" name="phone1" value="{{ old('phone1',  $student->phone1) }}">
        {{ $errors->first('phone1') }}
    </div>
    <div class="form-group">
        <label>Otro Teléfono (opcional): </label>
        <input type="text" name="phone2" value="{{ old('phone2',  $student->phone2) }}">
        {{ $errors->first('phone2') }}
    </div>
    <!--
    <div class="form-group">
        <label>Género: </label>
        <input type="text" name="gender" value="{{ old('$student->gender->description',  $student->gender) }}">
        {{ $errors->first('gender') }}
    </div>
    -->
    <div class="form-group">
        <label>Género: </label>
        <select type="select" name="gender" value="{{ old('gender') }}">
            @foreach ($genders as $gender)

            @if($student->gender == $gender->id)
                <option selected value="{{ $gender->id }}">{{ $gender->description }}</option>
            @else
                <option value="{{ $gender->id }}">{{ $gender->description }}</option>
            @endif

            @endforeach
        </select>
        {{ $errors->first('gender') }}
    </div>

    <div class="form-group">
        <label>Tipo de Documento: </label>
        <select type="select" name="document_type" value="{{ old('document_type') }}">
            @foreach ($documents as $document)

            @if($student->document_type == $document->id)
                <option selected value="{{ $document->id }}">{{ $document->description }}</option>
            
            @else
                <option value="{{ $document->id }}">{{ $document->description }}</option>
            @endif

            @endforeach
        </select>
        {{ $errors->first('document_type') }}
    </div>
    <!--
    <div class="form-group">
        <label>document_type: </label>
        <input type="text" name="document_type" value="{{ old('document_type',  $student->document_type) }}">
        {{ $errors->first('document_type') }}
    </div>
    -->
    <div class="form-group">
        <label>Nº Documento: </label>
        <input type="text" name="document" value="{{ old('document',  $student->document) }}">
        {{ $errors->first('document') }}
    </div>
    <!--
    <div class="form-group">
        <label>Foto: </label>
        <input type="text" name="photo" value="{{ old('photo',  $student->photo) }}">
        {{ $errors->first('photo') }}
    </div>
    -->
    <div class="form-group">
        <label>Fecha de Nacimiento: </label>
        <input type="text" name="birth_date" value="{{ old('birth_date',  $student->birth_date) }}">
        {{ $errors->first('birth_date') }}
    </div>

    <div class="form-group">
        <label>Ciudad Nacimiento: </label>
        <input type="text" name="birth_town" value="{{ old('birth_town',  $student->birth_town) }}">
        {{ $errors->first('birth_town') }}
    </div>

    <div class="form-group">
        <label>Provincia: </label>
        <input type="text" name="province" value="{{ old('province',  $student->province) }}">
        {{ $errors->first('province') }}
    </div>

    <div class="form-group">
        <label>País de Nacimiento: </label>
        <select type="select" name="birth_country_id" value="{{ old('birth_country_id') }}">
            @foreach ($countries as $country)

            @if($student->birth_country_id == $country->id)
                <option selected value="{{ $country->id }}">{{ $country->name }}</option>

            @else
                <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endif

            @endforeach
        </select>
        {{ $errors->first('birth_country_id') }}
    </div>

    <div class="form-group">
        <label>Tipo de Familia: </label>
        <select type="select" name="large_family_id" value="{{ old('large_family_id') }}">
            @foreach ($families as $family)

            @if($student->large_family_id == $family->id)

                <option selected value="{{ $family->id }}">{{ $family->name }}</option>

            @else
                <option value="{{ $family->id }}">{{ $family->name }}</option>
            @endif

            @endforeach
        </select>
        {{ $errors->first('large_family_id') }}
    </div>

    <input type="submit" value="Guardar" class="btn btn-primary">
    </form>
    </div>

@endsection('content')