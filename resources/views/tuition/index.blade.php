@extends('layouts.app')

@section('content')

    <h1>Listado de Matriculas</h1>

    @can('create', 'App\Tuition')
        <a href="/tuition/create"><button type="button" class="btn btn-success">Nuevo</button></a><br>
    @else
        No puedes dar altas!!
    @endcan

    <table class="table ">
        <thead>
            <tr>
                <th>Id</th>
                <th>Itinerario</th>
                <th>Estudiante</th>
                <th>Estudio</th>
                <th>Curso</th>
                <th>Prioridad</th>
                <th>Parcial</th>
                <th>Confrimado</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tuitions as $tuition)
                            
                <tr>
                    <td>  {{ $tuition->id }} </td>
                    <td>  {{ $tuition->itinerary->name }} </td>
                    <td>  {{ $tuition->student->name }} </td>
                    <td>  {{ $tuition->study->name }} </td>
                    <td>  {{ $tuition->course }} </td>
                    <td>  {{ $tuition->priority }} </td>
                    <td>  {{ $tuition->is_partial }} </td>
                    <td>  {{ $tuition->confirmed }} </td>
                    <td>  
                        <form method="post" action="/tuition/{{ $tuition->id }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}

                            @can('delete', $tuition)
                                <input type="submit" value="Borrar" class="btn btn-danger">
                                    
                                
                            @endcan
                            

                            @can('update', $tuition)
                                <a href="/tuition/{{ $tuition->id }}/edit"><button type="button" class="btn btn-warning">Editar</button></a>
                            @endcan

                            @can('view', $tuition)
                                <a href="/tuition/{{ $tuition->id }}"> <button type="button" class="btn btn-info">Ver</button></a>
                            @endcan
                        </form>


                    </td>
                </tr>
                
            @endforeach

        </tbody>
    </table>

    {{ $tuitions->render() }}

@endsection('content')