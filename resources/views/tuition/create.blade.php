@extends('layouts.app')

@section('content')

<h1>Detalle de matricula</h1>
<div class="form">
<form action="/tuitions" method="post">
    {{ csrf_field() }}

    
   <div class="form-group">
        <label>Itinerario: </label>
        <select type="select" name="id_itinerary" value="{{ old('id_itinerary') }}">
            @foreach ($itineraries as $itinerary)
            <option value="{{ $itinerary->id }}">{{ $itinerary->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_itinerary') }}
    </div>

    <div class="form-group">
        <label>Estudiante: </label>
        <select type="select" name="id_student" value="{{ old('id_student') }}">
            @foreach ($students as $student)
            <option value="{{ $student->id }}">{{ $student->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_student') }}
    </div>

    <div class="form-group">
        <label>Estudio: </label>
        <select type="select" name="id_study" value="{{ old('id_study') }}">
            @foreach ($studies as $study)
            <option value="{{ $study->id }}">{{ $study->name }}</option>
            @endforeach
        </select>
        {{ $errors->first('id_study') }}
    </div>

    <div class="form-group">
        <label>Curso: </label>
        <input type="text" name="course" value="{{ old('course') }}">
        {{ $errors->first('course') }}
    </div>

    <div class="form-group">
        <label>Prioridad: </label>
        <input type="text" name="priority" value="{{ old('priority') }}">
        {{ $errors->first('priority') }}
    </div>

    <div class="form-group">
        <label>Es parcial: </label>
        <input type="text" name="is_partial" value="{{ old('is_partial') }}">
        {{ $errors->first('is_partial') }}
    </div>

    <div class="form-group">
        <label>Confirmado: </label>
        <input type="text" name="confirmed" value="{{ old('confirmed') }}">
        {{ $errors->first('confirmed') }}
    </div>


    

    <div class="form-group">
        <input type="submit" value="Guardar" class="btn btn-primary">
    </div>
</form>
</div>

@endsection('content')