@extends('layouts.app')
@section('content')

    <h1>Editar datos de matricula</h1>
    <div class="form">
    <form  action="/tuition/{{ $tuition->id }}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">

    <div class="form-group">
        <label>Id: </label>
        <input type="text" name="id" value="{{ $tuition->id }}" readonly="readonly">
    </div>

    <div class="form-group">
        <label>Estudiante: </label>
        <input type="text" name="id_student" value="{{ $tuition->id_student }}" readonly="readonly" size="1">
        <input type="text" name="name_student" value="{{ $tuition->student->name  }}" readonly="readonly">
        {{ $errors->first('id_student') }}
    </div> 

    <div class="form-group">
        <label>Itinerario: </label>
        <select type="select" name="id_itinerary" value="{{ old('id_itinerary') }}">
            @foreach ($itineraries as $itinerary)
            <option value="{{ $itinerary->id }}" {{ $tuition->itinerary->id == $itinerary->id ?: old('itinerary') == $itinerary->id ? 'selected' : '' }}> {{ $itinerary->name }} </option>
            @endforeach
        </select>
        {{ $errors->first('id_itinerary') }}
    </div>

    <div class="form-group">
        <label>Estudio: </label>
        <select type="select" name="id_study" value="{{ old('id_study') }}">
            @foreach ($studies as $study)
            <option value="{{ $study->id }}" {{ $tuition->study->id == $study->id ?: old('study') == $study->id ? 'selected' : '' }} > {{ $study->name }} </option>
            @endforeach
        </select>
        {{ $errors->first('id_study') }}
    </div>

    <div class="form-group">
        <label>Course: </label>
        <input type="text" name="course" value="{{ old('course',  $tuition->course) }}">
        {{ $errors->first('course') }}
    </div>

    <div class="form-group">
        <label>Priority: </label>
        <input type="text" name="priority" value="{{ old('priority',  $tuition->priority) }}">
        {{ $errors->first('priority') }}
    </div>

    <div class="form-group">
        <label>Parcial: </label>
        <input type="text" name="is_partial" value="{{ old('is_partial',  $tuition->is_partial) }}">
        {{ $errors->first('is_partial') }}
    </div>

    <div class="form-group">
        <label>Confirmado: </label>
        <input type="text" name="confirmed" value="{{ old('confirmed',  $tuition->confirmed) }}">
        {{ $errors->first('confirmed') }}
    </div> 

    <input type="submit" value="Guardar" class="btn btn-primary">
    </form>
    </div>

@endsection('content')