@extends('layouts.app')
@section('content')

        <h1>Detalle de Matricula</h1>

        <p><b>Id:</b> {{ $tuition->id }} </p>
        <p><b>Itinerario:</b> {{ $tuition->itinerary->name }}</p>
        <p><b>Estudiante:</b> {{ $tuition->student->name }}</p>
        <p><b>Estudio:</b> {{ $tuition->study->name }}</p>
        <p><b>Curso:</b> {{ $tuition->course }}</p>
        <p><b>Prioridad:</b> {{ $tuition->priority }}</p>
        <p><b>Parcial:</b> {{ $tuition->is_partial }}</p>
        <p><b>Confirmado:</b> {{ $tuition->confirmed }}</p>

@endsection('content')