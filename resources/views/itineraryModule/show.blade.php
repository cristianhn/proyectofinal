@extends('layouts.app')
@section('content')
	@section('scripts')
<script type="text/javascript" src="/js/itineraryModule/itineraryModule-ajax.js"> 
</script>
@stop
	
		<form id="formModule"  action="/itinerary">
			{{ csrf_field() }}
			<div>
	    		<h4>Obligatorios (id)</h4>
	    		<ul class="list-group" id="list-module">		        		
	    			@foreach ($modules[0] as $value)
						<li class="list-group-item" id="module{{ $value->id }}">{{ $value->name }} ({{ $value->id }})</li>
				  	@endforeach						
				</ul>
				<a href="#" id="addModules" class="btn btn-success" data-toggle="modal" data-target="#modalLista" >Añadir un Modulo</a>

				<a href ="#" id="deleteModules" class="btn btn-danger" data-toggle="modal" data-target="#modalLista2">Quitar un modulo</a>
			</div>
    		@if ( ! empty($modules[1]) )	        			
    		<div>
    			<h4>Opcionales (id)</h4>
    			<ul class="list-group">		        		
        			@foreach ($modules[1] as $value)
					<li class="list-group-item">
						<div class="checkbox">
							<label><input type="checkbox" id="module{{ $value->id }}" value="{{ $value->name }} ({{ $value->id }})"></label>
						</div>							
					</li>
				  	@endforeach						
				</ul>
			</div>
			@endif
<br>
			<input type="submit" class="btn btn-primary" value="Guardar cambios" />
			<!--<a href="3" class="btn btn-primary">Guardar cambios</a>-->
		</form>
	

@endsection