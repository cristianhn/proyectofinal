@extends('layouts.app')
@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<form action="/itinerary/{{ $itinerary->id }}" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT"/>
				<div class="form-group">
					<label for="study">Estudio: </label>
					<select id="study" name="id_study" class="form-control">
						@foreach ($studies as $study)
							<option 
								value="{{$study->id}}" 
								{{ old('study') == $study->id ? 'selected' : '' }}>
								{{$study->name}}
							</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="name">Nombre: </label>
					<input type="text" class="form-control" name="name" id="name" value="{{ old('name', $itinerary->name) }}"/>
					@if ($errors->first('name'))
					<div class="alert alert-danger">
						<strong>¡Error!</strong> {{ $errors->first('name') }}
					</div>
					@endif
				</div>
				<input type="submit" class="btn btn-primary" value="Guardar cambios"/>
			</form>
		</div>
	</div>
@endsection