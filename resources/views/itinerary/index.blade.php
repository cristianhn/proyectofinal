@extends('layouts.app')
@section('content')
	<h1>Listado de itinerarios</h1>
	<a href="/itinerary/create" class="btn btn-success">Nuevo itinerario</a>
	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Estudio</th>
				<th>Nombre</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($itineraries as $itinerary)
				<tr>
					<td>{{ $itinerary->id }}</td>
					<td>{{ $itinerary->study->name }}</td>
					<td>{{ $itinerary->name }}</td>
					<td>
						<form class="form-horizontal" action="/itinerary/{{$itinerary->id}}" method="POST">
							{{csrf_field()}}
							<input type="hidden" name="_method" value="DELETE"/>
							<a href="/itinerary/{{$itinerary->id}}" class="btn btn-info">Ver</a>
							<a href="/itinerary/{{$itinerary->id}}/edit" class="btn btn-warning">Editar</a>
							<input type="submit" class="btn btn-danger" value="Borrar" />
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<div class="col-xs-12 text-center">
		{{ $itineraries->links() }}
	</div>
@endsection