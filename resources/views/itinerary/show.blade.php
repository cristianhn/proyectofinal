@extends('layouts.app')
@section('content')

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
		{{ csrf_field() }}
	        <h1>Detalle del itinerario</h1>
	        <p>Id: {{ $itinerary->id }}</p>
	        <p>Estudio: {{ $itinerary->study->name }}</p>
	        <p>Nombre: {{ $itinerary->name }}</p>
	        <h3>Modulos</h3>
	        <div class="col-sm-8 col-sm-offset-2">
	        	<a href="/itinerary/createItinerary/{{$itinerary->id}}" class="btn btn-primary">Modificar</a>
	        	<form>
	        		<h4>Obligatorios</h4>
	        		<ul class="list-group">		        		
	        			@foreach ($modules[0] as $value)
							<li class="list-group-item list-group-item-info">{{ $value->name }}</li>
					  	@endforeach						
					</ul>

	        		@if ( ! empty($modules[1]) )	        			
	        			<h4>Opcionales</h4>
	        			<ul class="list-group">		        		
		        			@foreach ($modules[1] as $value)
							<li class="list-group-item list-group-item-info">
								<div class="checkbox">
									<label><input type="checkbox" value="">{{ $value->name }}</label>
								</div>							
							</li>
						  	@endforeach						
						</ul>
					@endif
				</form>
	        	
	        </div>
	        

	    </div>
	</div>

@endsection('content')