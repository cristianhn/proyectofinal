$(document).ready(function () {

    /*Load Data List*/
    var loadData = function () {
        url = window.location.pathname;
        if(url == '/studyPlan'){
            $.get(url, "", function (data) {
                // alert(data);
                $("#tbodyMain").empty();
                for (var i = 0; i < data.studies.length; i++) {
                        var newRow = '<tr id="row' + data.studies[i].id + '" idRow=' + data.studies[i].id + ' >';
                        newRow = newRow + '<td > ' + data.studies[i].name + '</td>';
                        newRow = newRow + '<td data-study="' + data.studies[i].id + '">';
                        newRow = newRow + '<a class="btn btn-info display-plan">Ver plan completo</a> '; 
                        newRow = newRow + '</td>';
                        $("#tbodyMain").append(newRow);  
                }
            }, 'json');
        } else {
            $.get(url, "", function (data) {
                // alert(data);
                $("#tbodyMod").empty();
                var addButton = '<button class="btn btn-info add-item">A&ntilde;adir m&oacute;dulo</button></div></div>';
                for (var i = 0; i < data.studyPlans.data.length; i++){
                    var newRow = '<tr><td>' + data.studyPlans.data[i].module.code + '</td>';
                    newRow += '<td>' + data.studyPlans.data[i].module.name + '</td>';
                    newRow += '<td>' + data.studyPlans.data[i].year + '</td>';
                    newRow += '<td data-study="' + data.studyPlans.data[i].id_study + '" data-module="' + data.studyPlans.data[i].id_module + '"><button class="btn btn-danger remove-item" value="Borrar">Borrar</button></td></tr>';
                    $("#tbodyMod").append(newRow);
                }
                $("#boton").append(addButton);
            }, 'json');
        }
    };


    $("body").on("click",".display-plan", function(){
        var id = $(this).parent("td").data('study');
        url = window.location.pathname;
        $.get(url, "", function (data) {
            var studyPlans = data['studyPlans'];
            var study = data['studies'].filter(function(value){
                return value.id = id;
            })[0];
            studyPlans = data['studyPlans'].filter(function(value){
                return value.id_study == id;
            });
            $("div.modal" ).remove();
            var modal = '<div id="plan_' + id + '" class="modal" data-study="' + id + '"><div class="modal-dialog"><div class="modal-content animate">';
            modal += '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title">Plan de estudios del estudio ' + study.name +'</h4></div>';
            modal += '<div class="modal-body"><table class="table"><thead><tr><th>C&oacute;digo</th><th>M&oacute;dulo</th><th>Curso</th><th/></tr></thead><tbody id="tbodyMod">';
            for (var i = 0; i < studyPlans.length; i++){
                modal += '<tr><td>' + studyPlans[i].module.code + '</td>';
                modal += '<td>' + studyPlans[i].module.name + '</td>';
                modal += '<td>' + studyPlans[i].year + '</td>';
                modal += '<td data-study="' + id + '" data-module="' + studyPlans[i].id_module + '"><button class="btn btn-danger remove-item" value="Borrar">Borrar</button></td></tr>';
            }
            modal +='</tbody></table></div>';
            modal +='<div class="modal-footer"><button style="float: left" class="btn btn-info add-item">A&ntilde;adir m&oacute;dulo</button><button style="float: right" class="btn btn-info close-modal">Cerrar</button></div></div></div></div>';
            $(modal).insertAfter("table.table");
            console.log($("div.modal"));
            $("div.modal").modal('show');
        });
    });

    $("body").on("click", ".close-modal", function(){
        $("div.modal").modal('hide');
    });

    $("body").on("click",".add-item",function(){
        if (window.location.pathname == '/studyPlan'){
            var study = $(this).parent("div").parent("div").data('study');
        } else {
            var study = $("table").data('study');
        }
        url = '/studyPlan/create';
        $(this).prop("disabled", true);
        $.get(url, "", function (data) {
            var linea = '<tr><td><select id="module-code" name="code" class="form-control">';
            for (var i = 0; i < data.modules.length; i++){
                linea += '<option value="' + data.modules[i].id + '">' + data.modules[i].code + '</option>'; 
            }
            linea +='</select></td>';
            linea +='<td><select id="module-name" name="name" class="form-control">';
            for (var i = 0; i < data.modules.length; i++){
                linea += '<option value="' + data.modules[i].id + '">' + data.modules[i].name + '</option>'; 
            }
            linea +='</select></td>';
            linea +='<td><input type="text" id="year" name="year" class="form-control" placeholder="Curso"></input></td>';
            linea += '<td data-study="' + study + '" ><button class="btn btn-success submit-item" value="Guardar">Guardar</button>&nbsp;<button class="btn btn-danger remove-item" value="Borrar">Borrar</button></td></tr>';
            $("#tbodyMod").append(linea);
        });
    });

    $("body").on("change", "select[id^='module-']", function(){
        var selects = $("select");
        for (var i = 0; i < selects.length; i++){
            selects[i].selectedIndex = $(this)[0].selectedIndex;
        }
    });

    $("body").on("click", ".submit-item", function(){
        $("div.alert").remove();
        var tr = $(this).parent("td").parent("tr");
        var id_study = $(this).parent("td").data('study');
        var id_module = $("select#module-code")[0].value;
        var year = $("input#year")[0].value;
        var tabla = $("#tbodyMod").children();
        var continuar = true;
        for (var i = 0; i<tabla.length-1; i++){
            if ($(tabla[i].childNodes[3]).data('module') == id_module) {
                $('<div class="alert alert-danger"><strong>¡Error! </strong>El m&oacute;dulo seleccionado ya forma parte del plan de estudios.</div>').insertBefore("button.add-item");
                continuar = false;
            }
        }
        if (continuar){
            url = '/studyPlan';
            $.ajax({
                dataType: 'json',
                type: 'post',
                data: {'id_study' : id_study, 'id_module' : id_module, 'year' : year},
                url: url + '/',
                success: function(data){
                    var linea = '<td>' + $("select#module-code")[0].selectedOptions[0].text + '</td>';
                    linea +='<td>' + $("select#module-name")[0].selectedOptions[0].text + '</td>';
                    linea += '<td>' + year + '</td>';
                    linea += '<td data-study="' + id_study + '" data-module="' + id_module + '"><button class="btn btn-danger remove-item" value="Borrar">Borrar</button></td>';
                    tr.empty();
                    tr.append(linea);
                    $(".add-item").prop("disabled", false);
                },
                error: function(data){
                    var errors = data.responseJSON;
                    var elemento = tr.parent("tbody").parent("table").parent();
                    if ("id_study" in errors) {
                        $('<div class="alert alert-danger"><strong>¡Error! </strong>' + errors['id_study'][0] + '</div>').insertBefore("button.add-item");
                    }
                    if ("id_module" in errors) {
                        $('<div class="alert alert-danger"><strong>¡Error! </strong>' + errors['id_module'][0] + '</div>').insertBefore("button.add-item");
                    }
                    if ("year" in errors) {
                        $('<div class="alert alert-danger"><strong>¡Error! </strong>' + errors['year'][0] + '</div>').insertBefore("button.add-item");
                    }
                },
            });
        }
    });


    $("body").on("click",".remove-item",function(){
        $("div.alert").remove();
        var id_study = $(this).parent("td").data('study');
        var id_module = $(this).parent("td").data('module');
        var c_obj = $(this).parents("tr");
        url = '/studyPlan';
        $.ajax({
            dataType: 'json',
            type: 'delete',
            data: {'module' : id_module, 'study' : id_study},
            url: url + '/' + id_study,
        }).done(function(data){
            c_obj.fadeOut();
            $(".add-item").prop("disabled", false);
        });
    });



    loadData();

    /*Prepara cabeceras para envio del token csrf*/
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


})