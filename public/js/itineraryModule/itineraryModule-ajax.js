$(document).ready(function (){

	$("#addModules").click(function(){		
		$(".modal-body").text('');
		url = "/module";
		var modal='<div id="modalLista" class="modal fade" role="dialog"> <div class="modal-dialog">';
		modal +='<div class="modal-content"> <div class="modal-header"> <h4>Lista de modulos de todo el centro</h4></div>';
		modal += '<div class="modal-body" id="body-modal"></div>';
		modal += ' <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> </div>';
		modal += '</div></div></div>';

		var lista ='<ul class="list-group" id="listaModulos"></ul>';
		$("#formModule").append(modal);
		$("#body-modal").append(lista);
		//alert(modal);
		$.get(url,"",function(data){

			for(var x=0;x<data.modules.data.length;x++){

				$("#listaModulos").append("<li class='list-group-item'><a href='#' class = 'addModule' id=module"+data.modules.data[x].id+">"+data.modules.data[x].name+"</a></li>");
			}
		});
	});
	$("#deleteModules").click(function(){
		$(".modal-body").text('');
		var modal='<div id="modalLista2" class="modal fade" role="dialog"> <div class="modal-dialog">';
		modal +='<div class="modal-content"> <div class="modal-header"> <h4>Lista de modulos de este bloque</h4> </div>';
		modal += '<div class="modal-body" id="body-modal2"></div>';
		modal += ' <div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> </div>';
		modal += '</div></div></div>';

		var lista ='<ul class="list-group" id="listaModulos2"></ul>';
		$("#formModule").append(modal);
		$("#body-modal2").append(lista);

		$('#list-module > li').each(function(){
			$("#listaModulos2").append("<li class='list-group-item'><a href='#' class = 'deleteModule' id='"+$(this).prop('id')+"'>"+$(this).text()+"</a></li>");
		});		
	});
	

	$('#formModule').on('click','.addModule',function(){
		$('#list-module').append('<li class="list-group-item" id="module'+$(this).prop("id")+'">'+$(this).text()+' ('+$(this).prop("id").slice(6)+')</li>');
		$('#modalLista').modal('toggle');
	});
	$('#formModule').on('click','.deleteModule',function(){
		$('#'+$(this).prop('id')).remove();
		$('#modalLista2').modal('toggle');
	});




});