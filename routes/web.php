<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
    //return view('login');
//});

Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

//Route::resource('leadership', 'LeaderShipController');
//Route::resource('student', 'StudentController');

Route::resource('user', 'UserController');
Route::resource('tuition', 'TuitionController');
Route::resource('tuitions', 'TuitionController');
Route::resource('study', 'StudyController');
Route::resource('studies', 'StudyController');
Route::resource('module', 'ModuleController');
Route::resource('modules', 'ModuleController');
Route::resource('user', 'UserController');
Route::resource('student', 'StudentController');
Route::resource('students', 'StudentController');
Route::resource('studyPlan', 'StudyPlanController');

//Route::resource('module', 'ModuleController');
Route::resource('studyplan', 'StudyPlanController');
Route::resource('itinerary', 'ItineraryController');
Route::resource('itinerarymodule', 'ItineraryModuleController');
Route::resource('itinerary/createItinerary', 'ItineraryModuleController');
Route::resource('block', 'BlockController');







include "web/alberto.php";
include "web/cristian.php";
include "web/pedro.php";

Auth::routes();

Route::get('/home', 'HomeController@index');
