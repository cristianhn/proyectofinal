<?php

Route::resource('studies', 'StudyController');
Route::group(['middleware' => ['web']], function() {
  Route::resource('user','UserController');
  // more route will passed here
  Route::post ('/editItem', 'UserController@editItem');
  // add item routes
  Route::post ( '/addItem', 'UserController@addItem' );
  // delete item routes
  Route::post ( '/deleteItem', 'UserController@deleteItem' );
});
