<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LargeFamily extends Model
{
    protected $table = 'largeFamily';
}
