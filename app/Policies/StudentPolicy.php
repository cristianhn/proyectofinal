<?php

namespace App\Policies;

use App\User;
use App\Student;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Student $student)
    {
        //return true;
        return $user->isLeaderShip();
    }

    public function create(User $user)
    {
        return true;
        //return $user->isLeaderShip();
    }

    public function update(User $user, Student $student)
    {
        return $user->isLeaderShip();
    }
    public function delete(User $user, Student $student)
    {
        return $user->isLeaderShip();
    }
}
