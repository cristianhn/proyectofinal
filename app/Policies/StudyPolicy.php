<?php

namespace App\Policies;

use App\User;
use App\Study;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function view(User $user, Study $study)
    {
        //return true;
        return $user->isLeaderShip();
    }

    public function create(User $user)
    {
        //return true;
        return $user->isLeaderShip();
    }

    public function update(User $user, Study $study)
    {
        return $user->isLeaderShip();
    }
    public function delete(User $user, Study $study)
    {
        return $user->isLeaderShip();
    }
}
