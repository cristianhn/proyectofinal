<?php

namespace App\Policies;

use App\User;
use App\Tuition;
use Illuminate\Auth\Access\HandlesAuthorization;

class TuitionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function view(User $user, Tuition $tuition)
    {
        //return true;
        return $user->isLeaderShip();
    }

    public function create(User $user)
    {
        //return true;
        return $user->isLeaderShip();
    }

    public function update(User $user, Tuition $tuition)
    {
        return $user->isLeaderShip();
    }
    public function delete(User $user, Tuition $tuition)
    {
        return $user->isLeaderShip();
    }
}
