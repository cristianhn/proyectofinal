<?php

namespace App\Policies;

use App\User;

class UserPolicy extends BasePolicy
{
    /**
     * Determine whether the user can view the otherUser.
     *
     * @param  \App\User  $user
     * @param  \App\User  $otherUser
     * @return mixed
     */
    public function view(User $user, User $otherUser)
    {
        return $user->id === $otherUser->id;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the otherUser.
     *
     * @param  \App\User  $user
     * @param  \App\User  $otherUser
     * @return mixed
     */
    public function update(User $user, User $otherUser)
    {
        return $user->id === $otherUser->id;
    }

    /**
     * Determine whether the user can delete the otherUser.
     *
     * @param  \App\User  $user
     * @param  \App\User  $otherUser
     * @return mixed
     */
    public function delete(User $user, User $otherUser)
    {
        return $user->id === $otherUser->id;
    }
}
