<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyPlan extends Model
{

    protected $table = 'studyPlan';

    public function study()
    {
        return $this->belongsTo('App\Study', 'id_study');
    }

    public function module()
    {
        return $this->belongsTo('App\Module', 'id_module');
    }
}
