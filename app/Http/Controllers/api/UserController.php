<?php

namespace App\Http\Controllers\api;

use Auth;
use App\User;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        // Si es de jefatura...
        if ($user->can('viewAll', User::class)) {
            return User::with('type', 'student')->get();
        }

        // Si es un estudiante...
        return User::with('type', 'student')->where('id', $user->id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', User::class);

        $userData = $request->user;

        $user = User::create([
            'id_user_type' => 1,
            'name' => $userData['name'],
            'email' => $userData['email'],
            'password' => md5($userData['password'])
        ]);

        // Si es un estudiante...
        if ($studentData = $userData['student']) {
            $student = Student::create([
                'id_user' => $user->id,
                'name' => $studentData['name'],
                'surname1' => $studentData['surname1'],
                'surname2' => $studentData['surname2'],
                'phone1' => $studentData['phone1'],
                'phone2' => $studentData['phone2'],
                'gender' => $studentData['gender'],
                'document_type' => $studentData['document_type'],
                'document' => $studentData['document'],
                'birth_date' => $studentData['birth_date'],
                'birth_country_id' => $studentData['birth_country_id'],
                'province' => $studentData['province'],
                'birth_town' => $studentData['birth_town'],
                'large_family_id' => $studentData['large_family_id'],
                'large_family_doc' => $studentData['large_family_doc'],
                'birth_town' => $studentData['birth_town']
            ]);

            $student->code = $student->id;
            $student->save();

            $user->id_user_type = 2;
            $user->save();
        }

        return User::with('type', 'student')->findOrFail($user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('type', 'student')->findOrFail($id);

        $this->authorize('view', $user);
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::with('type', 'student')->findOrFail($id);

        $this->authorize('update', $user);

        $userData = $request->user;

        $user->name = $userData['name'];
        $user->email = $userData['email'];

        // Si es un estudiante...
        if ($user->id_user_type == 2) {
            $student = $user->student;
            $studentData = $userData['student'];

            $student->name = $studentData['name'];
            $student->surname1 = $studentData['surname1'];
            $student->surname2 = $studentData['surname2'];
            $student->phone1 = $studentData['phone1'];
            $student->phone2 = $studentData['phone2'];
            $student->gender = $studentData['gender'];
            $student->document_type = $studentData['document_type'];
            $student->document = $studentData['document'];
            $student->birth_date = $studentData['birth_date'];
            $student->birth_country_id = $studentData['birth_country_id'];
            $student->province = $studentData['province'];
            $student->birth_town = $studentData['birth_town'];
            $student->large_family_id = $studentData['large_family_id'];
            $student->large_family_doc = $studentData['large_family_doc'];

            $student->save();
        }

        $user->save();

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $this->authorize('delete', $user);

        $user->delete();

        return $id;
    }
}
