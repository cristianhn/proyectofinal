<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudyPlan;
use App\Study;
use App\Module;

class StudyPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $studies = Study::with('studyPlans')->orderBy('id')->get();
        $studyPlans = StudyPlan::with('module')->with('study')->orderBy('id_study')->orderBy('year')->orderBy('id_module')->get();
        if ($request->ajax()) {
            return ['studyPlans' => $studyPlans, 'studies' => $studies];
        } else {
            return view('studyPlan.index', ['studyPlans' => $studyPlans, 'studies' => $studies]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //$study = Study::findOrFail($request->study);
        $modules = Module::All();
        if ($request->ajax()) {
            return ['modules' => $modules];
        } else {
            return redirect('/studyPlan');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'id_study' => 'required|max:10',
        'id_module' => 'required|max:10',
        'year' => 'required|max:10',
        ]);
        $studyPlan = new StudyPlan();
        $studyPlan->id_study = $request->id_study;
        $studyPlan->id_module = $request->id_module;
        $studyPlan->year = $request->year;
        $studyPlan->save();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/studyPlan//' . $request->$id_study);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id_study)
    {
        $studyPlans = StudyPlan::where('id_study', $id_study)->with('module')->with('study')->paginate();
        if ($request->ajax()) {
            return ['studyPlans' => $studyPlans];
        } else {
            return view('studyPlan.show', ['studyPlans' => $studyPlans]);
        }
    }

    
    public function destroy(Request $request, $id_study)
    {
        StudyPlan::where('id_study', $request->study)->where('id_module', $request->module)->delete();
        if ($request->ajax()) {
            return response()->json(['done']);
        } else {
            return redirect('/studyPlan//' . $id_study);
        }
    }
}
