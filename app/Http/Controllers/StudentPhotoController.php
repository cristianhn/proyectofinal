<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use File;

class StudentPhotoController extends Controller
{
	const IMAGE_DESTINATION = 'storage/StudentPhotos';

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$student = Student::findOrFail($id);

        $this->authorize('update', $student);

    	$photo = $request->file('photo');
    	$photoName = md5($student->name . time() . rand()) 
    		. '.' . pathInfo($photo->getClientOriginalName())['extension'];
		$photoUrl = $photo->move(self::IMAGE_DESTINATION, $photoName);

        if (!$photoUrl) {
            return;
        }

        // Borramos la foto anterior.
        File::delete(self::IMAGE_DESTINATION . '/' . $student->photo);

        $student->photo = $photoName;
        $student->save();


		return $photoUrl;
    }
}
