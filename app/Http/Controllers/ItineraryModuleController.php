<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Block;
use App\ItineraryModule;
use App\Module;

class ItineraryModuleController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

    	
    }
    public function create()
    {
        return "s";
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->module1);
        return "asd";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
    	$block = Block::where('id_itinerary', $id)->get();
        $itineraryModule = ItineraryModule::where('id_itinerary',$id)->get();
        
        $modules_required=[];
        $modules_optional=[];
        $modules=[];
        $block=$itineraryModule[0]->id_block;
        foreach ($itineraryModule as $key => $value) {
            //dd($value->id_block);    
            if($value->id_block == $block){
                $module = Module::findOrFail($value->id_module);   
                array_push($modules_required, $module);
            }else{
                $module_opt=Module::findOrFail($value->id_module);
                array_push($modules_optional,$module_opt);
            }
        }
        array_push($modules,$modules_required);
        array_push($modules,$modules_optional);

        return view('itineraryModule.show', ['modules' => $modules]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
        return "";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    	return "";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        return "";
    }

}
