<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Block;
use App\Itinerary;
use App\Study;
use App\ItineraryModule;
use App\Module;


class ItineraryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itineraries = Itinerary::paginate(5);
        return view('itinerary.index', [
            'itineraries' => $itineraries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $studies = Study::all();
        return view('itinerary.create', ['studies' => $studies]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|max:50',
        'id_study' => 'required|max:10',
        ]);
        $itinerary = new Itinerary($request->all());
        $itinerary->save();
        return redirect('/itinerary');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $itinerary = Itinerary::findOrFail($id);
        $block = Block::where('id_itinerary', $id)->get();
        $itineraryModule = ItineraryModule::where('id_itinerary',$id)->get();
        
        $modules_required=[];
        $modules_optional=[];
        $modules=[];
        $block=$itineraryModule[0]->id_block;
        foreach ($itineraryModule as $key => $value) {
            //dd($value->id_block);    
            if($value->id_block == $block){
                $module = Module::findOrFail($value->id_module);   
                array_push($modules_required, $module);
            }else{
                $module_opt=Module::findOrFail($value->id_module);
                array_push($modules_optional,$module_opt);
            }
        }
        array_push($modules,$modules_required);
        array_push($modules,$modules_optional);
        //dd($modules);



        return view('itinerary.show', ['itinerary' => $itinerary, 'block'=>$block, 'modules'=>$modules]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $itinerary = Itinerary::findOrFail($id);
        $studies = Study::all();
        return view('itinerary.edit', ['itinerary' => $itinerary, 'studies' => $studies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'name' => 'required|max:50',
        'id_study' => 'required|max:10',
        ]);
        $itinerary = Itinerary::findOrFail($id);
        $itinerary->id_study = $request->id_study;
        $itinerary->name = $request->name;
        $itinerary->save();
        return redirect('/itinerary');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Itinerary::find($id)->delete();
        return redirect('/itinerary');
    }
    
    public function createItinerary(){

    }
    
}
