<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudyFormRequest;
use Illuminate\Http\Request;
use App\Study;
use App\StudyFamily;
use App\StudyType;

class StudyController extends Controller
{

    public function index(Request $request)
    {
        //return ('estamos en Study');
        $study = new Study();
        $studies = Study::paginate(4);
        return view('study.index', ['studies' => $studies]);

    }

    public function create()
    {
        $studiesFamily = StudyFamily::all();
        $studiesType = StudyType::all();
        $this->authorize('create', Study::class);
        return view('study.create', [
            'studiesFamily' => $studiesFamily,
            'studiesType' => $studiesType
            ]);
    }

    public function store(Request $request)
    {

        $this->authorize('create', Study::class);
        $study = new Study($request->all());
        $study->save();
        $id = $study->id;
        return redirect('/studies');
    }

    public function show($id)
    {
        $study = Study::findOrFail($id);
        $this->authorize('view', $study);
        return view('study.show', ['study' => $study]);
    }

    public function edit($id)
    {
        $studiesFamily = StudyFamily::all();
        $studiesType = StudyType::all();
        $study = Study::findOrFail($id);
        $this->authorize('update', $study);
        $studies = Study::all();
        return view('study.edit', [
            'study' => $study,
            'studies' => $studies,
            'studiesFamily' => $studiesFamily,
            'studiesType' => $studiesType
            ]);
    }

    public function update(Request $request, $id)
    {
               

        $study = Study::findOrFail($id);
        $this->authorize('update', $study);
        $study->id_study_family = $request->id_study_family;
        $study->id_study_type = $request->id_study_type;
        $study->code = $request->code;
        $study->name = $request->name;
        $study->short_name = $request->short_name;
        $study->short_name = $request->short_name;
        $study->abbreviation = $request->abbreviation;
        $study->save();
        return redirect('/study');
    }

    public function destroy($id)
    {
        $study = Study::findOrFail($id);
        $this->authorize('delete', $study);
        
        Study::destroy($id);
        return redirect('/study');
    }
}
