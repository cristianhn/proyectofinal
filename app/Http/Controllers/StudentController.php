<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentFormRequest;
use Illuminate\Http\Request;
use App\Student;
use App\User;
use App\Country;
use App\LargeFamily;
use App\DocumentType;
use App\Gender;

class StudentController extends Controller
{
    //
    public function index(Request $request)
    {
        
        $student = new Student();
        $students = Student::paginate(4);
        return view('student.index', ['students' => $students]);

    }

    public function create()
    {
        $users = User::all();
        $countries = Country::all();
        $families = LargeFamily::all();
        $documents = DocumentType::all();
        $genders = Gender::all();
        //$this->authorize('create', Student::class);
        return view('student.create', [            
            'users' => $users, 
            'countries' => $countries, 
            'families' => $families,
            'documents' => $documents, 
            'genders' => $genders
            ]);
    }

    public function store(Request $request)
    {
        //$this->authorize('create', Student::class);
        $student = new Student($request->all());
        switch ($student->large_family_id) {
            case '1':
                $student->large_family_doc = 0;
                break;
            
            case '2':
                $student->large_family_doc = 10;
                break;

            case '3':
                $student->large_family_doc = 20;
                break;
        }
        
        $student->save();
        $id = $student->id;
        return redirect('/students');
    }

    public function show($id)
    {
        $student = Student::findOrFail($id);
        $countries = Country::all();
        $genders = Gender::all();
        $users = User::all();
        $this->authorize('view', $student);
        return view('student.show', ['student' => $student,
            'countries' => $countries, 
            'genders' => $genders,
            'users' => $users]);
    }

    public function edit($id)
    {
        $student = Student::findOrFail($id);
        $genders = Gender::all();
        $countries = Country::all();
        $families = LargeFamily::all();
        $documents = DocumentType::all();

        $this->authorize('update', $student);
        $students = Student::all();
        return view('student.edit', [
            'student' => $student,
            'students' => $students,
            'genders' => $genders, 
            'countries' => $countries, 
            'families' => $families,
            'documents' => $documents, 
            ]);
    }

    public function update(Request $request, $id)
    {
               

        $student = Student::findOrFail($id);
        $this->authorize('update', $student);

        switch ($request->large_family_id) {
            case '1':
                $student->large_family_doc = 0;
                break;
            
            case '2':
                $student->large_family_doc = 10;
                break;

            case '3':
                $student->large_family_doc = 20;
                break;
        }
        $student->id = $request->id;
        $student->id_user = $request->id_user;
        $student->code = $request->code;
        $student->name = $request->name;
        $student->surname1 = $request->surname1;
        $student->surname2 = $request->surname2;
        $student->phone1 = $request->phone1;
        $student->phone2 = $request->phone2;
        $student->gender = $request->gender;
        $student->large_family_id = $request->large_family_id;
        $student->document_type = $request->document_type;
        $student->document = $request->document;
        $student->birth_date = $request->birth_date;
        $student->birth_town = $request->birth_town;
        $student->province = $request->province;
        $student->birth_country_id = $request->birth_country_id;
        $student->large_family_id = $request->large_family_id;
        
        $student->save();
        return redirect('/student');
    }

    public function destroy($id)
    {
        $student = Student::findOrFail($id);
        $this->authorize('delete', $student);
        
        Student::destroy($id);
        return redirect('/student');
    }
}
