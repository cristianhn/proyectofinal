<?php

namespace App\Http\Controllers;

use App\Http\Requests\TuitionFormRequest;
use Illuminate\Http\Request;
use App\Tuition;
use App\Itinerary;
use App\Study;
use App\Student;

class TuitionController extends Controller
{

    public function index(Request $request)
    {
        //return ('estamos en Tuition');
        $tuition = new Tuition();
        $tuitions = Tuition::paginate(4);
        return view('tuition.index', ['tuitions' => $tuitions]);

    }

    public function create()
    {
        $itineraries = Itinerary::all();
        $studies = Study::all();
        $students = Student::all();
        $this->authorize('create', Tuition::class);
        return view('tuition.create', [            
            'itineraries' => $itineraries,
            'studies' => $studies,
            'students' => $students
            ]);
    }

    public function store(TuitionFormRequest $request)
    {
        $this->authorize('create', Tuition::class);
        $tuition = new Tuition($request->all());
        $tuition->save();
        $id = $tuition->id;
        return redirect('/tuitions');
    }

    public function show($id)
    {
        $tuition = Tuition::findOrFail($id);
        $this->authorize('view', $tuition);
        return view('tuition.show', ['tuition' => $tuition]);
    }

    public function edit($id)
    {
        $itineraries = Itinerary::all();
        $studies = Study::all();
        $students = Student::all();
        $tuition = Tuition::findOrFail($id);
        $this->authorize('update', $tuition);
        $tuitions = Tuition::all();
        return view('tuition.edit', [
            'tuition' => $tuition,
            'tuitions' => $tuitions,
            'itineraries' => $itineraries,
            'studies' => $studies,
            'students' => $students
            ]);
    }

    public function update(TuitionFormRequest $request, $id)
    {
               

        $tuition = Tuition::findOrFail($id);
        $this->authorize('update', $tuition);
        $tuition->id_itinerary = $request->id_itinerary;
        $tuition->id_student = $request->id_student;
        $tuition->id_study = $request->id_study;
        $tuition->course = $request->course;
        $tuition->priority = $request->priority;
        $tuition->is_partial = $request->is_partial;
        $tuition->save();
        return redirect('/tuition');
    }

    public function destroy($id)
    {
        $tuition = Tuition::findOrFail($id);
        $this->authorize('delete', $tuition);
        
        Tuition::destroy($id);
        return redirect('/tuition');
    }
}
