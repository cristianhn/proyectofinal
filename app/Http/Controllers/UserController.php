<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\UserType;
use App\Student;
use App\Gender;
use App\DocumentType;
use App\Country;
use App\LargeFamily;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $genders = Gender::all();
        $documentTypes = DocumentType::all();
        $countries = Country::all();
        $largeFamilyTypes = LargeFamily::all();

        return view('user.index', [
            'user' => $user,
            'genders' => $genders,
            'documentTypes' => $documentTypes,
            'countries' => $countries,
            'largeFamilyTypes' => $largeFamilyTypes
        ]);
    }
}
