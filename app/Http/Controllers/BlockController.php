<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Block;
use App\Itinerary;

class BlockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blocks = Block::paginate();
        return view('block.index', [
            'blocks' => $blocks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $itineraries = Itinerary:: all();
        return view('block.create', ['itineraries' =>$itineraries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'id_itinerary' => 'required|max:100',
        'name' => 'required|max:100',
        'max' => 'required|max:6',
        'min' => 'required|max:6',
        ]);
        $block = new Block($request->all());
        $block->save();
        return redirect('/block');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $block = Block::findOrFail($id);
        return view('block.edit', ['block' => $block]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
        'id_itinerary' => 'required|max:100',
        'name' => 'required|max:100',
        'max' => 'required|max:6',
        'min' => 'required|max:6',
        ]);
        $block = Block::findOrFail($id);
        $block->id_itinerary = $request->id_itinerary;
        $block->name = $request->name;
        $block->max = $request->max;
        $block->min = $request->min;
        $block->save();
        return redirect('/block');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Block::find($id)->delete();
        return redirect('/block');
    }
}
