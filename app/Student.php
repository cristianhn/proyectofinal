<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Model
{
    protected $table = 'student';

    protected $fillable = ['id_user', 'code','name','surname1','surname2','phone1','phone2','gender','document_type','document','photo','birth_date','birth_country_id','province','birth_town','large_family_id','large_family_doc'];

    /**
    * Obtiene el país de nacimiento del estudiante.
    */
    public function country() {
        return $this->belongsTo('App\Country', 'birth_country_id');
    }

    /**
    * Obtiene los tutores del alumno.
    */
    public function parents() {
    	return $this->belongsToMany('App\Parents', 'studentParents', 'id_student', 'id_parents');
    }

    /**
    * Obtiene la dirección del estudiante.
    */
    public function address() {
    	return $this->hasOne('App\Address');
    }

}
