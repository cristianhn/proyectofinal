<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tuition extends Model
{
    protected $table = 'tuition';

    protected $fillable = [
        'id_itinerary', 'id_student', 'id_study', 'course', 'priority', 'is_partial', 'confirmed'
    ];

    public function student() {
    	return $this->belongsTo('App\Student', 'id_student');
    }

    public function study() {
    	return $this->belongsTo('App\Study', 'id_study');
    }

    public function itinerary() {
    	return $this->belongsTo('App\Itinerary', 'id_itinerary');
    }
}
