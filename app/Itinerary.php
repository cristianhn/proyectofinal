<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itinerary extends Model
{
    
    protected $table = 'itinerary';
    protected $fillable = ['id_study', 'name'];

    public function study()
    {
        return $this->belongsTo('App\Study', 'id_study');
    }

    public function blocks()
    {
        return $this->hasMany('App\Block', 'id_itinerary', 'id');
    }

    public function itineraryModules()
    {
        return $this->hasMany('App\ItineraryModule', 'id_itinerary', 'id');
    }
}
