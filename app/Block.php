<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Block extends Model
{
    protected $table="block";
    protected $fillable = ['id_itinerary', 'name','max','min'];
}
