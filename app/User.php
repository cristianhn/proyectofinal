<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user';
    
    protected $fillable = [
        'name', 'email', 'password','id_user_type', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * Obtiene el tipo asociado al usuario.
    */
    public function type() {
        return $this->belongsTo('App\UserType', 'id_user_type');
    }

    /**
    * Obtiene el estudiante asociado a un usuario.
    */
    public function student() {
        return $this->hasOne('App\Student', 'id_user');
    }

    public function isAdmin(){
        return $this->id_user_type === 1;
    }

    public function isLeaderShip()
    {
        return $this->id_user_type === 1;
    }
    public function isStudent()
    {
        return $this->id_user_type === 2;
    }
    
}
