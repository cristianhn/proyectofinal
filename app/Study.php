<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $table = 'study';
    protected $fillable = ['id_study_family', 'id_study_type', 'code', 'name', 'short_name', 'abbreviation'];

    /**
    * Obtiene la familia asociada al estudio
    */
    public function family()
    {
        return $this->belongsTo('App\StudyFamily', 'id_study_family');
    }

    /**
    * Obtiene el tipo asociado al estudio
    */
    public function type()
    {
        return $this->belongsTo('App\StudyType', 'id_study_type');
    }

    public function studyPlans()
    {
        return $this->hasMany('App\StudyPlan', 'id_study');
    }

    public function itineraries() {
        return $this->hasMany('App\Itinerary', 'id_study');
    }

    
}
