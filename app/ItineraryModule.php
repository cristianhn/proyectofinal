<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItineraryModule extends Model
{
    protected $table = 'itineraryModule';
    protected $fillable = ['id_itinerary', 'id_module', 'id_block'];
}
