<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudyFamily extends Model
{
    protected $table = 'studyFamily';

    public function studies() {
    	return $this->hasMany('App\Study', 'id_study_family');
    }
}
