<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    protected $table = 'module';
    protected $fillable = ['code', 'name'];

    public function studyPlans()
    {
        return $this->hasMany('App\StudyPlan', 'module_id', 'id');
    }

    public function itineraryModules()
    {
        return $this->hasMany('App\ItineraryModule', 'module_id', 'id');
    }
}
