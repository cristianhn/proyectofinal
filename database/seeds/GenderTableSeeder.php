<?php

use Illuminate\Database\Seeder;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders = ['Mujer', 'Hombre'];

        foreach ($genders as $gender) {
        	DB::table('gender')->insert([
        		'description' => $gender
    		]);
        }
    }
}
