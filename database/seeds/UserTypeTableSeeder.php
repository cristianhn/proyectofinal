<?php

use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['jefatura', 'alumno'];

        foreach ($types as $type) {
            DB::table('userType')->insert([
                'description' => $type,
            ]);
        }
    }
}
