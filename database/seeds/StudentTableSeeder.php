<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $studentUsers = DB::table('user')->where('id_user_type', '=', '2')->get();

        foreach ($studentUsers as $studentUser) {
            $name = $faker->firstName();
            $surname1 = $faker->lastName;
            $surname2 = $faker->lastName;
        	DB::table('student')->insert([
        		'id_user' => $studentUser->id,
        		'code' => $faker->randomNumber(6),
        		'name' => $name,
        		'surname1' => $surname1,
        		'surname2' => $surname2,
        		'phone1' => $faker->numerify('#########'),
        		'phone2' => $faker->numerify('#########'),
        		'gender' => rand(1, 2),
        		'document_type' => rand(1, 2),
        		'document' => $faker->randomNumber(8) . strtoupper($faker->randomLetter),
        		'photo' => null,
        		'birth_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        		'birth_country_id' => rand(1, 239),
        		'province' => $faker->state,
        		'birth_town' => $faker->city,
        		'large_family_id' => rand(1, 3),
                'large_family_doc' => $faker->randomNumber(6)
    		]);
            $user = DB::table('user')->where('id', '=', $studentUser->id);
            $user->update(['name' => $name . " " . $surname1 . " " . $surname2]);
        }
    }
}
