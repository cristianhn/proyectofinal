<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class ParentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
        $students = DB::table('student')->get();

 		foreach ($students as $student) {
 			DB::table('parents')->insert([
 				'document' => $faker->randomNumber(8) . strtoupper($faker->randomLetter),
 				'authorized' => rand(0, 1),
 				'name' => $faker->firstName,
 				'surname1' => $faker->lastName,
 				'surname2' => $faker->lastName,
 				'relationship' => rand(1, 3),
 				'phone' => $faker->numerify('#########'),
 				'email' => $faker->email,
 				'lives_with' => $faker->name
			]);
 		}
    }
}
