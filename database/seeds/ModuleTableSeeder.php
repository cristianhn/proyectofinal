<?php

use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $modules = [
        	[
        		'code' => 'SO',
        		'name' => 'Sistemas informáticos'
        	],
        	[
        		'code' => 'BD',
        		'name' => 'Bases de datos'
        	],
        	[
        		'code' => 'PR',
        		'name' => 'Programación'
        	],
        	[
        		'code' => 'LM',
        		'name' => 'Lenguajes de marcas y sistemas de gestión de información'
        	],
        	[
        		'code' => 'ED',
        		'name' => 'Entornos de desarrollo'
        	],
        	[
        		'code' => 'DC',
        		'name' => 'Desarrollo web en entorno cliente'
        	],
        	[
        		'code' => 'DS',
        		'name' => 'Desarrollo web en entorno servidor'
        	],
        	[
        		'code' => 'DA',
        		'name' => 'Despliegue de aplicaciones web'
        	],
        	[
        		'code' => 'DW',
        		'name' => 'Diseño de interfaces web'
        	],
        	[
        		'code' => 'FOL',
        		'name' => 'Formacion y orientacion laboral'
        	],
        	[
        		'code' => 'EIE',
        		'name' => 'Empresa e iniciativa emprendedora'
        	]
        ];

        foreach ($modules as $module) {
        	DB::table('module')->insert([
        		'code' => $module['code'],
        		'name' => $module['name']
    		]);
        }
    }
}
