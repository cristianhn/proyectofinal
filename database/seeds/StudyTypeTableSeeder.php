<?php

use Illuminate\Database\Seeder;

class StudyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Presencial', 'A distancia'];

        foreach ($types as $type) {
        	DB::table('studyType')->insert([
        		'name' => $type
        	]);
        }
    }
}
