<?php

use Illuminate\Database\Seeder;

class LargeFamilySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['no_familia_numerosa','general', 'especifica'];

        foreach ($types as $type) {
            DB::table('largeFamily')->insert([
                'name' => $type,
            ]);
        }
    }
}
