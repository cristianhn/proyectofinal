<?php

use Illuminate\Database\Seeder;
use App\Student;

class StudentParentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = Student::all();

        foreach ($students as $student) {
    		DB::table('studentParents')->insert([
    			'id_student' => $student->id,
    			'id_parents' => $student->id
			]);
        }
    }
}
