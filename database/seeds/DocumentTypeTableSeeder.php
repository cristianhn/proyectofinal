<?php

use Illuminate\Database\Seeder;

class DocumentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['DNI', 'NIE'];

        foreach ($types as $type) {
            DB::table('documentType')->insert([
                'description' => $type,
            ]);
        }
    }
}
