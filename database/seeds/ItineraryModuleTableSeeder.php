<?php

use Illuminate\Database\Seeder;

class ItineraryModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$itineraryModules = [
    		// Primer curso de DAW
    		1 => [
    			// Primer y único bloque
    			1 => [1, 2, 3, 5, 10]
    		],
    		// Segundo curso de DAW
    		2 => [
    			// Primer y único bloque
    			2 => [4, 6, 7, 8, 9, 11]
    		]
    	];

    	foreach ($itineraryModules as $idItinerary => $blocks) {
    		foreach ($blocks as $idBlock => $modules) {
    			foreach ($modules as $idModule) {
    				DB::table('itineraryModule')->insert([
    					'id_itinerary' => $idItinerary,
    					'id_module' => $idModule,
    					'id_block' => $idBlock
					]);
    			}
    		}
    	}
    }
}
