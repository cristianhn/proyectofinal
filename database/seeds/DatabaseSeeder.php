<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*** STUDENT ***/
        $this->call(LargeFamilySeeder::class);
        $this->call(UserTypeTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(DocumentTypeTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(GenderTableSeeder::class);
        $this->call(StudentTableSeeder::class);
        $this->call(AddressTableSeeder::class);
        $this->call(RelationshipTableSeeder::class);
        $this->call(ParentsTableSeeder::class);
        $this->call(StudentParentsTableSeeder::class);
        /*** TUITION ***/
        $this->call(ModuleTableSeeder::class);
        $this->call(StudyFamilyTableSeeder::class);
        $this->call(StudyTypeTableSeeder::class);
        $this->call(StudyTableSeeder::class);
        $this->call(StudyPlanTableSeeder::class);
        $this->call(ItineraryTableSeeder::class);
        $this->call(BlockTableSeeder::class);
        $this->call(ItineraryModuleTableSeeder::class);
        $this->call(TuitionTableSeeder::class);
    }
}
