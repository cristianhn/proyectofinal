<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
            'id' => 11,
            'id_user_type' => 1,            
            'name' => 'pepe',
            'email' => 'pepe@gmail.com',
            'password' => bcrypt('pepe'),
            'api_token' => md5(bcrypt('pepe'))
        ]);

        $faker = Faker::create();

    	foreach (range(1,10) as $index) {
	        DB::table('user')->insert([
	        	'id_user_type' => rand(1, 2),
	            'name' => $faker->name,
	            'email' => $faker->email,
	            'password' => bcrypt('secret'),
                'api_token' => md5(bcrypt('secret'))
	        ]);
        }
    }
}
