<?php

use Illuminate\Database\Seeder;

class ItineraryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itineraries = [
        	[
        		'study' => 1,
        		'name' => 'Primer curso de DAW'
        	],
            [
                'study' => 1,
                'name' => 'Segundo curso de DAW'
            ]
        ];

        foreach ($itineraries as $itinerary) {
        	DB::table('itinerary')->insert([
    			'id_study' => $itinerary['study'],
    			'name' => $itinerary['name']
    		]);
        }
    }
}
