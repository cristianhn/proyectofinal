<?php

use Illuminate\Database\Seeder;

class StudyFamilyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $families = [
        	'Actividades Físicas y Deportivas',
        	'Administración y Gestión',
        	'Agraria',
        	'Artes Gráficas',
        	'Artes y Artesanías',
        	'Comercio y Marketing',
        	'Edificación y Obra Civil',
        	'Electricidad y Electrónica',
        	'Energía y Agua',
        	'Fabricación Mecánica',
        	'Hostelería y Turismo',
        	'Imagen Personal',
        	'Imagen y Sonido',
        	'Industrias Alimentarias',
        	'Industrias Extractivas',
        	'Informática y Comunicaciones',
        	'Instalación y Mantenimiento',
        	'Madera, Mueble y Corcho',
        	'Marítimo-Pesquera',
        	'Química',
        	'Sanidad',
        	'Seguridad y Medio Ambiente',
        	'Servicios Socioculturales y a la Comunidad',
        	'Textil, Confección y Piel',
        	'Transporte y Mantenimiento de Vehículos',
        	'Vidrio y Cerámica'
        ];

        foreach ($families as $family) {
        	DB::table('studyFamily')->insert([
        		'name' => $family
    		]);
        }
    }
}
