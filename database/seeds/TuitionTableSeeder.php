<?php

use Illuminate\Database\Seeder;
use App\Student;
use App\Study;

class TuitionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $students = Student::all();

        foreach ($students as $student) {
        	$study = Study::with('itineraries')->get()->random(1)->first();
        	$itinerary = $study->itineraries->first();

        	DB::table('tuition')->insert([
        		'id_itinerary' => $itinerary->id,
        		'id_student' => $student->id,
        		'id_study' => $study->id,
        		'course' => rand(0, 1) ? 'Primero' : 'Segundo',
        		'priority' => 1,
                'is_partial' => rand(0, 1) === 1,
        		'confirmed' => rand(0, 1) === 1
    		]);
        }
    }
}
