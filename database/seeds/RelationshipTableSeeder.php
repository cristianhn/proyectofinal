<?php

use Illuminate\Database\Seeder;

class RelationshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $relationships = ['Padre', 'Madre', 'Pariente', 'Tutor'];

        foreach ($relationships as $relation) {
        	DB::table('relationship')->insert([
        		'description' => $relation
    		]);
        }
    }
}
