<?php

use Illuminate\Database\Seeder;

class BlockTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itineraryBlocks = [
        	// Primer curso de DAW
        	1 => [
    			[
    				'name' => 'Obligatorias',
    				'min' => 6,
    				'max' => 6
        		]
        	],
        	// Segundo curso de DAW
        	2 => [
        		[
        			'name' => 'Obligatorias',
        			'min' => 6,
        			'max' => 6
        		]
        	]
        ];

        foreach ($itineraryBlocks as $idItinerary => $blocks) {
        	foreach ($blocks as $block) {
        		DB::table('block')->insert([
        			'id_itinerary' => $idItinerary,
        			'name' => $block['name'],
        			'min' => $block['min'],
        			'max' => $block['max']
    			]);
        	}
        }
    }
}
