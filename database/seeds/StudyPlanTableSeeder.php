<?php

use Illuminate\Database\Seeder;

class StudyPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studyPlans = [
            // DAW
            1 => range(1, 11)
        ];

        foreach ($studyPlans as $idStudy => $studyModules) {
            foreach ($studyModules as $module) {
                DB::table('studyPlan')->insert([
                    'id_study' => $idStudy,
                    'id_module' => $module,
                    'year' => rand(1, 4)
                ]);
            }
        }
    }
}
