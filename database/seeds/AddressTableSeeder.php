<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $students = DB::table('student')->get();

        foreach ($students as $student) {
        	DB::table('address')->insert([
        		'student_id' => $student->id,
        		'province' => $faker->state,
        		'habitual' => $faker->state,
        		'town' => $faker->city,
        		'path' => $faker->streetAddress,
        		'other' => substr($faker->address, 0, 49),
        		'code' => intval($faker->postcode)
    		]);
        }
    }
}
