<?php

use Illuminate\Database\Seeder;

class StudyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $studies = [
        	[
        		'family' => 16,
        		'type' => 1,
        		'code' => 'IFC303',
        		'name' => 'Desarrollo de aplicaciones web',
        		'short_name' => 'Programación web',
        		'abbreviation' => 'DAW'
        	]
        ];

        foreach ($studies as $study) {
        	DB::table('study')->insert([
        		'id_study_family' => $study['family'],
        		'id_study_type' => $study['type'],
        		'code' => $study['code'],
        		'name' => $study['name'],
        		'short_name' => $study['short_name'],
        		'abbreviation' => $study['abbreviation']
    		]);
        }
    }
}
