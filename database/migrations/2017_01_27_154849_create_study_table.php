<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_study_family')->unsigned();
            $table->integer('id_study_type')->unsigned();
            $table->string('code', 6)->unique();
            $table->string('name', 50);
            $table->string('short_name', 20);
            $table->string('abbreviation', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study');
    }
}
