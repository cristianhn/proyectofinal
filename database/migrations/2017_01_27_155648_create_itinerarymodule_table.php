<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerarymoduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itineraryModule', function (Blueprint $table) {
            $table->integer('id_itinerary')->unsigned();
            $table->integer('id_module')->unsigned();
            $table->integer('id_block')->unsigned();
            $table->timestamps();
        });
        DB::unprepared('ALTER TABLE `itineraryModule` ADD PRIMARY KEY (`id_itinerary`, `id_module`)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itineraryModule');
    }
}
