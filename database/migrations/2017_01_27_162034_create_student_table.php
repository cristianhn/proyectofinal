<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned()->unique();
            $table->integer('code');
            $table->string('name', 50);
            $table->string('surname1', 50);
            $table->string('surname2', 50)->nullable();
            $table->string('phone1', 9);
            $table->string('phone2', 9)->nullable();
            $table->integer('gender')->unsigned();
            $table->integer('document_type')->unsigned();
            $table->string('document');
            $table->string('photo', 50)->nullable();
            $table->date('birth_date');
            $table->integer('birth_country_id')->unsigned();
            $table->string('province', 50);
            $table->string('birth_town', 50);
            $table->integer('large_family_id')->unsigned();
            $table->string('large_family_doc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
