<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*** STUDENT ***/
        // User
        Schema::table('user', function($table){
            $table->foreign('id_user_type')->references('id')->on('userType');
        });
        // Student
        Schema::table('student', function($table){
            $table->foreign('id_user')->references('id')->on('user')->onDelete('cascade');
            $table->foreign('gender')->references('id')->on('gender');
            $table->foreign('document_type')->references('id')->on('documentType');
            $table->foreign('birth_country_id')->references('id')->on('country');
            $table->foreign('large_family_id')->references('id')->on('largeFamily');
        });
        // Parents
        Schema::table('parents', function($table){
            $table->foreign('relationship')->references('id')->on('relationship');
        });
        // StudentParents
        Schema::table('studentParents', function($table){
            $table->foreign('id_student')->references('id')->on('student')->onDelete('cascade');
            $table->foreign('id_parents')->references('id')->on('parents')->onDelete('cascade');
        });
        // Address
        Schema::table('address', function($table){
            $table->foreign('student_id')->references('id')->on('student')->onDelete('cascade');
        });
        /*** TUITION ***/
        // Study
        Schema::table('study', function($table){
            $table->foreign('id_study_family')->references('id')->on('studyFamily');
            $table->foreign('id_study_type')->references('id')->on('studyType');
        });
        // StudyPlan
        Schema::table('studyPlan', function($table){
            $table->foreign('id_study')->references('id')->on('study')->onDelete('cascade');
            $table->foreign('id_module')->references('id')->on('module')->onDelete('cascade');
        });
        // Itinerary
        Schema::table('itinerary', function($table){
            $table->foreign('id_study')->references('id')->on('study')->onDelete('cascade');
        });
        // Block
        Schema::table('block', function($table){
            $table->foreign('id_itinerary')->references('id')->on('itinerary')->onDelete('cascade');
        });
        // ItineraryModule
        Schema::table('itineraryModule', function($table){
            $table->foreign('id_itinerary')->references('id')->on('itinerary');
            $table->foreign('id_module')->references('id')->on('module');
            $table->foreign('id_block')->references('id')->on('block');
        });
        // Tuition
        Schema::table('tuition', function($table){
            $table->foreign('id_itinerary')->references('id')->on('itinerary');
            $table->foreign('id_student')->references('id')->on('student')->onDelete('cascade');
            $table->foreign('id_study')->references('id')->on('study');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*** STUDENT ***/
        // User
        Schema::table('user', function($table){
            $table->dropForeign('user_id_user_type_foreign');
        });
        // Student
        Schema::table('student', function($table){
            $table->dropForeign('student_id_user_foreign');
            $table->dropForeign('student_gender_foreign');
            $table->dropForeign('student_document_type_foreign');
            $table->dropForeign('student_large_family_id_foreign');
            $table->dropForeign('student_birth_country_id_foreign');
        });
        // Parents
        Schema::table('parents', function($table){
            $table->dropForeign('parents_relationship_foreign');
        });
        // StudentParents
        Schema::table('studentParents', function($table){
            $table->dropForeign('studentparents_id_student_foreign');
            $table->dropForeign('studentparents_id_parents_foreign');
        });
        // Address
        Schema::table('address', function($table){
            $table->dropForeign('address_student_id_foreign');
        });
        /*** TUITION ***/
        // Study
        Schema::table('study', function($table){
            $table->dropForeign('study_id_study_family_foreign');
            $table->dropForeign('study_id_study_type_foreign');
        });
        // StudyPlan
        Schema::table('studyPlan', function($table){
            $table->dropForeign('studyPlan_id_study_foreign');
            $table->dropForeign('studyPlan_id_module_foreign');
        });
        // Itinerary
        Schema::table('itinerary', function($table){
            $table->dropForeign('itinerary_id_study_foreign');
        });
        // Block
        Schema::table('block', function($table){
            $table->dropForeign('block_id_itinerary_foreign');
        });
        // ItineraryModule
        Schema::table('itineraryModule', function($table){
            $table->dropForeign('itineraryModule_id_itinerary_foreign');
            $table->dropForeign('itineraryModule_id_module_foreign');
            $table->dropForeign('itineraryModule_id_block_foreign');
        });
        // Tuition
        Schema::table('tuition', function($table){
            $table->dropForeign('tuition_id_itinerary_foreign');
            $table->dropForeign('tuition_id_student_foreign');
            $table->dropForeign('tuition_id_study_foreign');
        });
    }
}
