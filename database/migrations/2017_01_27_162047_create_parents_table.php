<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('document', 50);
            $table->boolean('authorized');
            $table->string('name', 50);
            $table->string('surname1', 50);
            $table->string('surname2', 50)->nullable();
            $table->integer('relationship')->unsigned();
            $table->string('phone', 9);
            $table->string('email', 50)->unique();
            $table->string('lives_with');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents');
    }
}
